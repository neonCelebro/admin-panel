import axios from 'axios';
import {showSpin} from "./store/actions/actions";

axios.interceptors.request.use(function (config) {
   showSpin(true);
    return config;
}, function (error) {
    return Promise.reject(error);
});

axios.interceptors.response.use(function (response) {
    showSpin(false);
    return response;
}, function (error) {
    return Promise.reject(error);
});

export default axios;