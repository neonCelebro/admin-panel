import React from "react";
import { NotificationContainer } from "react-notifications";
import "react-notifications/lib/notifications.css";
import ToolBar from "../Components/Toolbar";

export default function Layout (props) {
    const {children, user} = props;
    return (
            <div>
                <header>
                    {user && <ToolBar/>}
                </header>
                {children}
                <NotificationContainer />
            </div>
    );
}