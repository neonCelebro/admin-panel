import React, {useEffect} from "react";
import Container from "@material-ui/core/Container";
import {useStyles} from "../styles";
import User from "../Components/User";
import {useSelector} from "react-redux";
import {fetchUsers} from "../store/actions/userActions";


export default function Users () {
    const users = useSelector(state => state.users);
    console.log(users);
    // useEffect(  () => {
    //     const fetchData = async () => {
    //         await fetchUsers();
    //     };
    //     fetchData();
    // }, []);
    const classes = useStyles();
    const {usersRoot} = classes;
    return (
        <Container maxWidth='md' className={usersRoot}>
            <User/>
            <User/>
            <User/>
            <User/>
            <User/>
            <User/>
            <User/>
            <User/>
        </Container>
    )
}