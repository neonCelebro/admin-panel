import React, {Fragment, useContext, useState} from 'react';
import Button from "@material-ui/core/Button";

import {useStyles} from "../../styles";
import {getStateForSubmitForm, validateSubmitForm} from "../../manager";

import {loginUser} from "../../store/actions/userActions";

export default function SubmitForm(props) {
    const {btnConfig, setError, children, action} = props;
    const [values, setValues] = useState(getStateForSubmitForm(children));
    const {variant, title, color} = btnConfig;
    const classes = useStyles();
    const {container, button} = classes;

    const handleChange = event => {
        const {name, value} = event.target;
        setValues({...values, [name]: value});
    };

    const submitForm = e => {
        e.preventDefault();
        const isError = validateSubmitForm(values, children);
        if (isError) {
            setError(true);
        } else {
            setError(false);
            action(values);
            setValues(getStateForSubmitForm(children))
        }
    };

    return (
        <Fragment>
            <form
                className={container}
                onChange={handleChange}
                onSubmit={submitForm}
                noValidate
            >
                {children}
                <Button
                    variant={variant}
                    color={color}
                    className={button}
                    type="submit"
                >
                    {title}
                </Button>
            </form>
        </Fragment>
    );
}
