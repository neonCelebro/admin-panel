import React from 'react';
import './App.css'
import Layout from "./Containers/Layout";
import Routes from "./Routes";
import {navigate, useRoutes} from "hookrouter";
import {useSelector} from "react-redux";
import Preloader from "./Components/Preloader";


export default function App() {
    const routes = useRoutes(Routes);
    const {userData, showSpin} = useSelector(state => state);
    console.log(userData);
    (userData && userData.role !== 'admin') && navigate('/login');
    return (
        <div className="App">
            {
                showSpin ? <Preloader/> :
                    <Layout user={userData}>
                        {routes}
                    </Layout>
            }
        </div>
    );
};
