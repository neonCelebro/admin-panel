import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
import FaceIcon from '@material-ui/icons/Face';
import {useStyles} from "../styles";

export default function Chips() {
    const classes = useStyles();
    const {chip} = classes;

    function handleDelete() {
        alert('You clicked the delete icon.');
    }

    function handleClick() {
        alert('You clicked the Chip.');
    }

    return (
            <Chip
                avatar={
                    <Avatar>
                        <FaceIcon />
                    </Avatar>
                }
                label="Clickable Deletable Chip"
                onClick={handleClick}
                onDelete={handleDelete}
                className={chip}
            />
    );
}