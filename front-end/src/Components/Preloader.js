import React from 'react';
import {useStyles} from '../styles';
import src from '../escalade.gif';

export default function Preloader () {
    const classes = useStyles();
    const {preloader} = classes;
    return (
        <img className={preloader} src={src} alt=""/>
    )
}