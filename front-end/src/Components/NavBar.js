import React from 'react';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import SupervisedUserCircleIcon from '@material-ui/icons/SupervisedUserCircle';
import CategoryIcon from '@material-ui/icons/Category';
import LibraryBooksIcon from '@material-ui/icons/LibraryBooks';
import {A} from 'hookrouter';
import {useStyles} from "../styles";



export default function NavBar() {
    const classes = useStyles();
    const {navRoot} = classes;
    return (
        <BottomNavigation
            showLabels
            className={navRoot}
        >
            <BottomNavigationAction label="users"  icon={<A href='/users'><SupervisedUserCircleIcon /></A>} />
           <BottomNavigationAction label="categories" icon={<A href='/categories'><CategoryIcon /></A>}/>
            <BottomNavigationAction label="articles" icon={<A href='/articles'><LibraryBooksIcon /></A>} />
        </BottomNavigation>
    );
}