import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import {useStyles} from "../styles";
import NavBar from "./NavBar";
import {userlogOut} from "../store/actions/userActions";

export default function ToolBar() {
    const classes = useStyles();
    const {root, menuButton, title} = classes;
    return (
        <div className={root}>
            <AppBar position="static">
                <Toolbar>
                    <IconButton edge="start" className={menuButton} color="inherit" aria-label="menu">
                        hello, admin!
                    </IconButton>
                    <Typography variant="h6" className={title}>
                        <NavBar/>
                    </Typography>
                    <Button onClick={userlogOut} color="inherit"><ExitToAppIcon/>LogOut</Button>
                </Toolbar>
            </AppBar>
        </div>
    );
}