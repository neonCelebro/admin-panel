import React, {useState} from 'react';
import TextField from "@material-ui/core/TextField";
import SubmitForm from "../Containers/SubmitForm/SubmitForm";
import {useStyles} from "../styles";
import {loginUser} from "../store/actions/userActions";

export default function LoginForm() {

    let [error, setError] = useState(false);

    const btnConfig = {
        color: 'primary',
        variant: 'outlined',
        title: 'Log in'
    };
    const classes = useStyles();
    const {main, textField} = classes;

    return (
        <div className={main}>
            <SubmitForm
                setError={setError}
                error={error}
                btnConfig={btnConfig}
                action={loginUser}
            >
                <TextField
                    required
                    error={error}
                    label="Email"
                    id="email"
                    className={textField}
                    type="email"
                    name="email"
                    defaultValue=""
                    autoComplete="email"
                    margin="normal"
                    variant="outlined"
                />
                <TextField
                    required
                    error={error}
                    id="password"
                    label="Password"
                    className={textField}
                    type="password"
                    name="password"
                    defaultValue=""
                    autoComplete="password"
                    margin="normal"
                    variant="outlined"
                />
            </SubmitForm>
        </div>
    )
}