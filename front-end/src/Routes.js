import React from 'react';
import Login from "./Components/LogInForm";
import Users from "./Containers/Users";

export default {
    '/login': () => <Login/>,
    '/users': () => <Users/>,

};

