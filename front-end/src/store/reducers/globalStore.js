import { createStore } from 'redux';
import {assignAll, createReducer} from 'redux-act';
import * as actions from '../actions/actions'
import {loadState, saveState} from "../localStorage";

let userData = loadState();
userData = Object.keys({...userData}).length ? {...userData.userData} : null;
export const initialStore = {
    showSpin: false,
    userData,
    users: [],
};


const reducer = createReducer({
    [actions.showSpin]: (state, action) => ({...state, showSpin : action}),
    [actions.logIn]: (state, action) => ({...state, userData: action}),
    [actions.logOut]: (state) => ({...state, userData: null}),
    [actions.fetchUsers]: (state, action) => ({...state, users: action}),
}, initialStore); // <-- This is the default state

// Create the store
const store = createStore(reducer);
assignAll(actions, store);

store.subscribe(() => {
    const userData = store.getState().userData;
    console.log(userData);
    saveState({
        userData
    })
});

export default store;
