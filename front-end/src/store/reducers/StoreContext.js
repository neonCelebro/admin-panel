import React, { createContext } from 'react';
import { reducer, store } from './globalStore';
import useThunkReducer from "react-hook-thunk-reducer";

const StoreContext = createContext(store);

const StoreProvider = ({ children }) => {
    const [state, actions] = useThunkReducer(reducer, store);
    return (
        <StoreContext.Provider
            value={{ state, actions }}
        >
            {children}
        </StoreContext.Provider>
    );
};

export { StoreContext, StoreProvider };