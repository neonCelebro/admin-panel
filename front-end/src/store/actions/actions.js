import {createAction} from "redux-act";

export const showSpin = createAction('show spinner', isShow => isShow);
export const logIn = createAction('login user success', userData => userData);
export const logOut = createAction('log out user success');
export const fetchUsers = createAction('fetch users', usersArray => usersArray);