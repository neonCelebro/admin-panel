import axios from '../../axiosConfig';
import {NotificationManager} from 'react-notifications';
import {navigate} from "hookrouter";

import {logIn, showSpin, logOut} from "./actions";
import config from "../../config";


export const loginUser = params => {
    return axios.post(`${config.url}/users/sessions`, params)
        .then(
        response => {
            const {message, user} = response.data;
            logIn(user);
            NotificationManager.success(message);
            navigate('/users');
        }, error => {
            navigate('/login');
            const {message} = error.response.data;
            NotificationManager.error(message);
        })
};
export const userlogOut = () => {
    return axios.delete(`${config.url}/users/sessions`).then(
        response => {
            const {message} = response.data;
            logOut();
            NotificationManager.success(message);
            navigate('/');
        }, error => {
            const {message} = error.response.data;
            NotificationManager.error(message);
        })
};

export const fetchUsers = () => {
    showSpin(true);
    return axios.get(`${config.url}/users/users`).then(
        response => {
            const {usersArray} = response.data;
            fetchUsers(usersArray);
        }, error => {
            showSpin(false);
            const {message} = error.response.data;
            console.log(message);
            NotificationManager.error(message);
        })
        .finally(() => showSpin(false));
};


