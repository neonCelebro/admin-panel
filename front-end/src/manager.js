export const getStateForSubmitForm = (childrens) => {
    const state = {};
    childrens.forEach(children => {
        state[children.props.name] = children.props.defaultValue
    });
    return state;
};

export const validateSubmitForm = (values, childrens) => {
    const valuesArray = Object.keys(values);
    let showError = false;
    valuesArray.forEach((value, id) => {
        if (!values[value] && childrens[id].props.required) showError = true;
    });
    return showError;
};