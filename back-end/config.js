const path = require('path');

const rootPath = __dirname;

let config = {
    rootPath,
    uploadPath: path.join(rootPath, '/public/uploads'),
    db: {
        url: 'mongodb://localhost:27017',
        name: "admin_panel"
    },
};

module.exports = config;

