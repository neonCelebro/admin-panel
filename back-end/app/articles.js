const express = require('express');
const auth = require('../middleware/auth');
const config = require('../config');
const nanoid = require("nanoid");
const multer = require('multer');
const path = require('path');

const User = require('../models/User');
const Place = require('../models/Place');



const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});


const createRouter = () => {
    const router = express.Router();

    router.post('/', [auth, upload.single('image')], async (req, res) => {
        try {
            const newPlace = req.body;
            if (req.file) {
                newPlace.titleImage = req.file.filename;
            }
            const place = new Place(newPlace);

            await place.save();
            const places = await Place.find({isDeleted: false});
            res.send(places)
        } catch (e) {
            res.status(500).send({message: 'Невозможно сохранить секцию в данный момент'})
        }
    });

    router.post('/rate', [auth,upload.single('image')], async (req, res) => {
        try {
            const place = await Place.findById(req.query.id);

            if (req.file) {
                place.image.push(req.file.filename);
            }

            place.rate.push(req.body.ratingData);
            place.passed ++;
            place.calcAverageFood(place.rate);
            place.calcAverageService(place.rate);
            place.calcAverageInterior(place.rate);

            await place.save();
            res.send(place);
        } catch (e) {
            console.log(e);
            res.send({message: 'В данный момент невозможно сохранить данные.', e})
        }
    });

    router.get('/', async (req, res) => {
            try {
                const sections = await Place.find({isDeleted: false});
                return res.send(sections);
            } catch (e) {
                res.status(400).send({message: 'Невозможно загрузить секции в данный момент'});
            }
        });
    router.get('/place', async (req, res) => {
        try {
            const section = await Place.findById(req.query.id).populate("user");
            return res.send(section);
        } catch (e) {
            res.status(400).send({message: 'Невозможно загрузить секции в данный момент'});
        }
    });

    return router;
};

module.exports = createRouter;