const express = require("express");
const User = require("../models/User");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const createRouter = () => {
    const router = express.Router();

    router.post("/sessions", async (req, res) => {
        try {
            const user = await User.findOne({email: req.body.email});

            if (!user) {
                return res.status(400).send({message: "Имя пользователя не найдено"});
            }

            const isMatch = await user.checkPassword(req.body.password);

            if (!isMatch) {
                return res.status(400).send({message: "Неверный пароль!"});
            }

            user.generateToken();
            await user.save();

            return res.send({message: "Успешный вход в систему", user});
        } catch (e) {
            res.send({message: "Неверный пароль или имя пользователя!"});
        }
    });


    router.delete("/sessions", async (req, res) => {
        try {
            const token = req.get("Token");
            const success = {message: "Успешный выход"};

            if (!token) return res.send(success);

            const user = await User.findOne({token});

            if (!user) return res.send(success);

            user.generateToken();
            await user.save();

            return res.send({message:success});
        } catch (e) {
            res
                .status(500)
                .send({message: "Невозможно выполнить выход в данный момент!"});
        }
    });
    router.get("/users", [auth, permit('admin')], async (req, res) => {
        try {
            const users = await User.find().$where(req.user.token !== this.token);
            console.log(users);
            res.send(users);
        } catch (e) {
            res.send({message: 'some went wrong!'}).status(500);
        }
    });

    return router;
};

module.exports = createRouter;
