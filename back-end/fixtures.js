const mongoose = require('mongoose');
const config = require('./config');

const User = require('./models/User');


mongoose.connect(`${config.db.url}/${config.db.name}`, {useNewUrlParser: true});

const db = mongoose.connection;

const dropCollection = async (collectionName) => {
    try {
        await db.dropCollection(collectionName);
    } catch (e) {
        console.log(`Collection ${collectionName} did not present, skipping drop...`)
    }
};

const collections = ['users'];

db.once('open', async () => {
    collections.forEach(collectionName => (
        dropCollection(collectionName)
    ));

    const [admin, user] = await User.create({
            email: 'user1@mail.ru',
            password: 'admin',
            role: 'admin',
            token: ''
        },
        {
            email: 'vasa@mail.ru',
            password: 'user',
            role: 'user',
            token: ''
        },
        {
            email: 'petya@mail.ru',
            password: 'user',
            role: 'user',
            token: ''
        },
        {
            email: 'masha@mail.ru',
            password: 'user',
            role: 'user',
            token: ''
        },
        {
            email: 'etya@mail.ru',
            password: 'user',
            role: 'user',
            token: ''
        });

    db.close();
});