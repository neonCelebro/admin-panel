const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SectionSchema = new Schema({
    parent_id: Schema.Types.ObjectId,
    title: {
        type: String,
        required: true
    },
    isActive: {
        type: Boolean,
        default: true
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
});

const Article = mongoose.model('Section', SectionSchema);
module.exports = Article;