const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const SectionSchema = new Schema({
    category_id: {
        type: Schema.Types.ObjectId,
        ref: 'Category'
    },
    user_id: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    title: {
        type: String,
        required: true
    },
    isActive: {
        type: Boolean,
        default: true
    },
    image: {
        type: String
    },
    description: {
        type: String,
        required: true
    },
    isDeleted: {
        type: Boolean,
        default: false
    },
});

const Article = mongoose.model('Section', SectionSchema);
module.exports = Article;